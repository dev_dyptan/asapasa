import Alamofire
import AlamofireObjectMapper

public class ApiManager {
    //API URL https://api.asapasa.kz/api/v[version_number]/[method]
    private let BASE_URL = "https://api.asapasa.kz/api/v1"
//    private let BASE_URL = "https://private-ba07f-podoroge.apiary-mock.com/v1"  //TODO URL для тестирования
    var headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": ""
    ]

    static let getInstance = ApiManager()

    private init() {
    }

    func authenticate(email: String, password: String, deviceToken: String,
                      success: @escaping (AuthenticateResponce) -> (), failure: ((Error) -> ())) {
        let parameters: Parameters = [
                "auth-provider": "email",
                "email": email,
                "password": password,
                "device_token": deviceToken,
                "device_type": "ios"
        ]
        let url = BASE_URL + "/auth/authenticate"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<AuthenticateResponce>) in

            switch response.result {
            case .success:
                print("Authenticate Successful")
                let token = response.result.value?.token
                self.headers["Authorization"] = "Bearer \(String(describing: token))"
                success(response.result.value!)

            case .failure(let error):
                print("Authenticate Error:\(error)")
            }
        }
    }


    func signup(email: String, password: String, deviceToken: String, phone: String, name: String,
                success: @escaping (AuthenticateResponce) -> (), failure: ((Error) -> ())) {
        let parameters: Parameters = [
                "email": email,
                "password": password,
                "phone": phone,
                "name": name,
                "device_type": "ios",
                "device_token": deviceToken
        ]
        let url = BASE_URL + "/auth/signup"

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<SignupResponce>) in

            switch response.result {
            case .success:
                print("Signup Successful")

            case .failure(let error):
                print("Signup Error: \(error)")
            }
        }
    }

    func logout(token: String, success: @escaping (SignupResponce) -> ()?, failure: (Error) -> ()?) {
        headers["Authorization"] = ""
        let url = BASE_URL + "/auth/logout"
        Alamofire.request(url, method: .post, headers: headers).validate()
                .responseObject { (response: DataResponse<SignupResponce>) in

                    switch response.result {
                    case .success:
                        print("Logout Successful")

                    case .failure(let error):
                        print("Logout Error: \(error)")
                    }
                }

    }

    func pickup(city: String, size: String, price: String, distance: String, nearby: String, page: String,
                success: @escaping (SignupResponce) -> ()?, failure: (Error) -> ()?) {
        let parameters: Parameters = [
                "pickup": city,
                "size": size,
                "price": price,
                "distance": distance,
                "nearby": nearby,
                "page": page
        ]
        let url = BASE_URL + "/tasks"

        Alamofire.request(url, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers)
                .validate().responseObject { (response: DataResponse<TaskObj>) in

                    switch response.result {
                    case .success:
                        print("Task Successful")

                    case .failure(let error):
                        print("Task Error: \(error)")
                    }
                }
    }

    func task(taskOrder: Data, success: @escaping (TaskObj) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks"
        let pickup = taskOrder.pickup!
        let delivery = taskOrder.delivery!

        let parameters: Parameters = [
                "title": taskOrder.title!,
                "pickup.country": pickup.country!,
                "pickup.region": pickup.region!,
                "pickup.city": pickup.city!,
                "pickup.street": pickup.street!,
                "pickup.location": pickup.location!,
                "pickup.lat": pickup.lat!,
                "pickup.lon": pickup.lon!,
                "delivery.country": delivery.country!,
                "delivery.region": delivery.region!,
                "delivery.city": delivery.city!,
                "delivery.street": delivery.street!,
                "delivery.location": delivery.location!,
                "delivery.lat": delivery.lat!,
                "delivery.lon": delivery.lon!,
                "distance": taskOrder.distance!,
                "delivery_date": taskOrder.deliveryDate!,
                "delivery_time_from": taskOrder.deliveryTimeFrom!,
                "delivery_time_to": taskOrder.deliveryTimeTo!,
                "size_id": taskOrder.sizeId!,
                "price": taskOrder.price!,
                "images": taskOrder.images!
        ]

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<TaskObj>) in

            switch response.result {
            case .success:
                print("Task Successful")

            case .failure(let error):
                print("Task Error: \(error)")
            }
        }
    }

    func taskInbox(type: String, success: @escaping (TaskObj) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/inbox/"

        let parameters: Parameters = [
                "type": type
        ]

        Alamofire.request(url, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers)
                .validate().responseObject { (response: DataResponse<TaskObj>) in

                    switch response.result {
                    case .success:
                        print("inbox Successful")

                    case .failure(let error):
                        print("inbox Error: \(error)")
                    }
                }
    }

    func taskOutbox(type: String, success: @escaping (TaskObj) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/outbox/"

        let parameters: Parameters = [
                "type": type
        ]

        Alamofire.request(url, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers)
                .validate().responseObject { (response: DataResponse<TaskObj>) in

                    switch response.result {
                    case .success:
                        print("inbox Successful")

                    case .failure(let error):
                        print("inbox Error: \(error)")
                    }
                }
    }

    func TaskById(id: Int, success: @escaping (TaskObj) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/"

        let parameters: Parameters = [
                "id": id
        ]

        Alamofire.request(url, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers)
                .validate().responseObject { (response: DataResponse<TaskObj>) in

                    switch response.result {
                    case .success:
                        print("inbox Successful")

                    case .failure(let error):
                        print("inbox Error: \(error)")
                    }
                }
    }

    func changeTask(taskOrder: Data, success: @escaping (TaskObj) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/id"
        let pickup = taskOrder.pickup!
        let delivery = taskOrder.delivery!

        let parameters: Parameters = [
                "title": taskOrder.title!,
                "pickup.country": pickup.country!,
                "pickup.region": pickup.region!,
                "pickup.city": pickup.city!,
                "pickup.street": pickup.street!,
                "pickup.location": pickup.location!,
                "pickup.lat": pickup.lat!,
                "pickup.lon": pickup.lon!,
                "delivery.country": delivery.country!,
                "delivery.region": delivery.region!,
                "delivery.city": delivery.city!,
                "delivery.street": delivery.street!,
                "delivery.location": delivery.location!,
                "delivery.lat": delivery.lat!,
                "delivery.lon": delivery.lon!,
                "distance": taskOrder.distance!,
                "delivery_date": taskOrder.deliveryDate!,
                "delivery_time_from": taskOrder.deliveryTimeFrom!,
                "delivery_time_to": taskOrder.deliveryTimeTo!,
                "size_id": taskOrder.sizeId!,
                "price": taskOrder.price!,
                "images": taskOrder.images!
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<TaskObj>) in

            switch response.result {
            case .success:
                print("changeTask Successful")

            case .failure(let error):
                print("changeTask Error: \(error)")
            }
        }
    }

    func requestToCarry(id: Int, success: @escaping (TaskObj) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/task/\(id)/request-to-carry"

        Alamofire.request(url, method: .put, encoding: URLEncoding(destination: .httpBody), headers: headers).validate()
                .responseObject { (response: DataResponse<StatusResponse>) in

                    switch response.result {
                    case .success:
                        print("requestToCarry Successful")

                    case .failure(let error):
                        print("requestToCarry Error: \(error)")
                    }
                }
    }

    func deleteTask(id: Int, success: @escaping (StatusResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/"

        let parameters: Parameters = [
                "id": id
        ]

        Alamofire.request(url, method: .delete, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<StatusResponse>) in

            switch response.result {
            case .success:
                print("deleteTask Successful")

            case .failure(let error):
                print("deleteTask Error: \(error)")
            }
        }
    }

    func assignCourier(id: Int, offerId: Int, success: @escaping (StatusResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/\(id)/assign-courier"

        let parameters: Parameters = [
                "offer_id": offerId
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<StatusResponse>) in

            switch response.result {
            case .success:
                print("assignCourier Successful")

            case .failure(let error):
                print("assignCourier Error: \(error)")
            }
        }
    }

    func declineCourier(id: Int, offerId: Int, success: @escaping (StatusResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/\(id)/decline-courier"

        let parameters: Parameters = [
                "offer_id": offerId
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<StatusResponse>) in

            switch response.result {
            case .success:
                print("declineCourier Successful")

            case .failure(let error):
                print("declineCourier Error: \(error)")
            }
        }
    }

    func itemDelivered(id: Int, success: @escaping (StatusResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/\(id)/item-delivered"

        Alamofire.request(url, method: .put, headers: headers).validate()
                .responseObject { (response: DataResponse<StatusResponse>) in

                    switch response.result {
                    case .success:
                        print("declineCourier Successful")

                    case .failure(let error):
                        print("declineCourier Error: \(error)")
                    }
                }
    }

    func confirmAcceptance(id: Int, success: @escaping (StatusResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/\(id)/confirm-acceptance"

        Alamofire.request(url, method: .put, headers: headers).validate()
                .responseObject { (response: DataResponse<StatusResponse>) in

                    switch response.result {
                    case .success:
                        print("confirmAcceptance Successful")

                    case .failure(let error):
                        print("confirmAcceptance Error: \(error)")
                    }
                }
    }

    func priceOfferFromCourier(id: Int, newPrice: Int, success: @escaping (StatusResponse) -> ()?,
                               failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/\(id)/price-offer-from-courier"

        let parameters: Parameters = [
                "new_price": newPrice
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<StatusResponse>) in

            switch response.result {
            case .success:
                print("confirmAcceptance Successful")

            case .failure(let error):
                print("confirmAcceptance Error: \(error)")
            }
        }
    }

    func acceptPriceFromCourier(id: Int, newPrice: Int, success: @escaping (StatusResponse) -> ()?,
                                failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/\(id)/accept-price-from-courier"

        let parameters: Parameters = [
                "new_price": newPrice
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<StatusResponse>) in

            switch response.result {
            case .success:
                print("acceptPriceFromCourier Successful")

            case .failure(let error):
                print("acceptPriceFromCourier Error: \(error)")
            }
        }
    }

    func rejectPriceFromCourier(id: Int, success: @escaping (StatusResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/tasks/\(id)/reject-price-from-courier"

        Alamofire.request(url, method: .put, headers: headers).validate()
                .responseObject { (response: DataResponse<StatusResponse>) in

                    switch response.result {
                    case .success:
                        print("rejectPriceFromCourier Successful")

                    case .failure(let error):
                        print("rejectPriceFromCourier Error: \(error)")
                    }
                }
    }

    func notifications(success: @escaping (StatusResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/notifications"

        Alamofire.request(url, headers: headers).validate()
                .responseObject { (response: DataResponse<NotificationsResponse>) in

                    switch response.result {
                    case .success:
                        print("notifications Successful")

                    case .failure(let error):
                        print("notifications Error: \(error)")
                    }
                }
    }

    func reviews(receiverId: Int, title: String, msg: String, mark: Int, type: Int,
                 success: @escaping (NotificationsResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/reviews"

        let parameters: Parameters = [
                "receiver_id": receiverId,
                "title": title,
                "message": msg,
                "mark": mark,
                "type": type
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<ReviewResponce>) in

            switch response.result {
            case .success:
                print("reviews Successful")

            case .failure(let error):
                print("reviews Error: \(error)")
            }
        }
    }

    func reviewsEdit(id: Int, title: String, msg: String, mark: Int, success: @escaping (ReviewResponce) -> ()?,
                     failure: (Error) -> ()?) {
        let url = BASE_URL + "/reviews/"

        let parameters: Parameters = [
                "id": id,
                "title": title,
                "message": msg,
                "mark": mark
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<ReviewResponce>) in

            switch response.result {
            case .success:
                print("reviewsEdit Successful")

            case .failure(let error):
                print("reviewsEdit Error: \(error)")
            }
        }
    }

    func getReviewsById(id: Int, success: @escaping (ReviewResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/reviews/"

        let parameters: Parameters = [
                "id": id
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<ReviewResponce>) in

            switch response.result {
            case .success:
                print("getReviewsById Successful")

            case .failure(let error):
                print("getReviewsById Error: \(error)")
            }
        }
    }

    func deleteReview(id: Int, success: @escaping (ReviewResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/reviews/"

        let parameters: Parameters = [
                "id": id
        ]

        //        Alamofire.request(url, method: .delete, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers).validate().response(completionHandler:{(DefaultDataResponse) in
        //
        //            switch response.result {
        //                case .success:
        //                    print("deleteReview Successful")
        //
        //                case .failure(let error):
        //                    print("deleteReview Error: \(error)")
        //            }
        //        }
    }

    func getCards(success: @escaping (CardsResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/cards"

        Alamofire.request(url, headers: headers).validate().responseObject { (response: DataResponse<CardsResponce>) in

            switch response.result {
            case .success:
                print("getReviewsById Successful")

            case .failure(let error):
                print("getReviewsById Error: \(error)")
            }
        }
    }


    func setCards(anchor: String, last: String, success: @escaping (CardsResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/cards"

        let parameters: Parameters = [
                "rebill_anchor": anchor,
                "last4": last
        ]

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<CardsResponce>) in

            switch response.result {
            case .success:
                print("reviewsEdit Successful")

            case .failure(let error):
                print("reviewsEdit Error: \(error)")
            }
        }
    }

    func deleteCards(id: Int, success: @escaping (CardsResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/cards/"

        let parameters: Parameters = [
                "id": id
        ]

        Alamofire.request(url, method: .delete, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<CardsResponce>) in

            switch response.result {
            case .success:
                print("deleteCards Successful")

            case .failure(let error):
                print("deleteCards Error: \(error)")
            }
        }
    }

    func setDefCards(id: Int, success: @escaping (CardsResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/cards/\(id)/"

        Alamofire.request(url, method: .put, headers: headers).validate()
                .responseObject { (response: DataResponse<CardsResponce>) in

                    switch response.result {
                    case .success:
                        print("setDefCards Successful")

                    case .failure(let error):
                        print("setDefCards Error: \(error)")
                    }
                }
    }

    func currentUser(success: @escaping (ProfileResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/me"

        Alamofire.request(url, headers: headers).validate()
                .responseObject { (response: DataResponse<ProfileResponce>) in

                    switch response.result {
                    case .success:
                        print("currentUser Successful")

                    case .failure(let error):
                        print("currentUser Error: \(error)")
                    }
                }
    }

    func getUserById(id: Int, success: @escaping (ProfileResponce) -> ()?, failure: (Error) -> ()?) {

        let parameters: Parameters = [
                "id": id
        ]


        Alamofire.request(BASE_URL, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<ProfileResponce>) in

            switch response.result {
            case .success:
                print("getUserById Successful")

            case .failure(let error):
                print("getUserById Error: \(error)")
            }
        }
    }

    func editProfileCurrentUser(email: String, phone: String, password: String,
                                success: @escaping (ProfileResponce) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/me"

        let parameters: Parameters = [
                "email": email,
                "phone": phone,
                "password": password
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<ProfileResponce>) in

            switch response.result {
            case .success:
                print("editProfileCurrentUser Successful")

            case .failure(let error):
                print("editProfileCurrentUser Error: \(error)")
            }
        }
    }

    func getAllChatCurrentUser(success: @escaping (ConversationsResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/conversations"

        Alamofire.request(url, headers: headers).validate()
                .responseObject { (response: DataResponse<ConversationsResponse>) in

                    switch response.result {
                    case .success:
                        print("getAllChatCurrentUser Successful")

                    case .failure(let error):
                        print("getAllChatCurrentUser Error: \(error)")
                    }
                }
    }

    func createdChat(recepientId: Int, success: @escaping (ConversationsResponse) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/conversations"

        let parameters: Parameters = [
                "recepient_id": recepientId
        ]

        Alamofire.request(url, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers)
                .validate().responseObject { (response: DataResponse<ConversationsResponse>) in

                    switch response.result {
                    case .success:
                        print("createdChat Successful")

                    case .failure(let error):
                        print("createdChat Error: \(error)")
                    }
                }
    }

    func getMsgChat(id: Int, success: @escaping (Chat) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/conversations/"

        let parameters: Parameters = [
                "id": id
        ]

        Alamofire.request(url, parameters: parameters, encoding: URLEncoding(destination: .httpBody), headers: headers)
                .validate().responseObject { (response: DataResponse<Chat>) in


                    switch response.result {
                    case .success:
                        print("setDefCards Successful")

                    case .failure(let error):
                        print("setDefCards Error: \(error)")
                    }
                }
    }

    func sendMsgChat(id: Int, msg: String, success: @escaping (Chat) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/conversations/\(id)"

        let parameters: Parameters = [
                "message": msg
        ]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: URLEncoding(destination: .httpBody),
                headers: headers).validate().responseObject { (response: DataResponse<Chat>) in


            switch response.result {
            case .success:
                print("setDefCards Successful")

            case .failure(let error):
                print("setDefCards Error: \(error)")
            }
        }
    }


    func clearChat(id: Int, msg: String, success: @escaping (Chat) -> ()?, failure: (Error) -> ()?) {
        let url = BASE_URL + "/conversations/\(id)"

        Alamofire.request(url, headers: headers).validate().responseObject { (response: DataResponse<Chat>) in


            switch response.result {
            case .success:
                print("setDefCards Successful")

            case .failure(let error):
                print("setDefCards Error: \(error)")
            }
        }
    }

    func lostSend(id: Int) {
        let url = BASE_URL + "/conversations/\(id)/received"

        Alamofire.request(url, headers: headers).response { (DefaultDataResponse) in
        }

    }
}
