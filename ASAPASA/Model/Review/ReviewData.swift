//
//  Data.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class ReviewData: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let mark = "mark"
    static let id = "id"
    static let links = "_links"
    static let sender = "sender"
    static let createdAt = "created_at"
    static let title = "title"
    static let message = "message"
  }

  // MARK: Properties
  public var mark: Int?
  public var id: Int?
  public var links: ReviewLinks?
  public var sender: ReviewSender?
  public var createdAt: String?
  public var title: String?
  public var message: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    mark <- map[SerializationKeys.mark]
    id <- map[SerializationKeys.id]
    links <- map[SerializationKeys.links]
    sender <- map[SerializationKeys.sender]
    createdAt <- map[SerializationKeys.createdAt]
    title <- map[SerializationKeys.title]
    message <- map[SerializationKeys.message]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = mark { dictionary[SerializationKeys.mark] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    if let value = sender { dictionary[SerializationKeys.sender] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    return dictionary
  }

}
