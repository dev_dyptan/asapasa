//
//  AuthenticateResponce.swift
//
//  Created by Colt on 03.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class AuthenticateResponce: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let phone = "phone"
    static let name = "name"
    static let email = "email"
    static let id = "id"
    static let token = "token"
    static let links = "_links"
  }

  // MARK: Properties
  public var phone: String?
  public var name: String?
  public var email: String?
  public var id: Int?
  public var token: String?
  public var links: AuthLinks?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    phone <- map[SerializationKeys.phone]
    name <- map[SerializationKeys.name]
    email <- map[SerializationKeys.email]
    id <- map[SerializationKeys.id]
    token <- map[SerializationKeys.token]
    links <- map[SerializationKeys.links]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = token { dictionary[SerializationKeys.token] = value }
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    return dictionary
  }

}
