//
//  Update.swift
//
//  Created by Colt on 03.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class Update: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let desc = "desc"
    static let href = "href"
  }

  // MARK: Properties
  public var desc: String?
  public var href: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    desc <- map[SerializationKeys.desc]
    href <- map[SerializationKeys.href]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = desc { dictionary[SerializationKeys.desc] = value }
    if let value = href { dictionary[SerializationKeys.href] = value }
    return dictionary
  }

}
