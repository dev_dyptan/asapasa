//
//  Chat.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class Chat: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let data = "data"
    static let from = "from"
    static let total = "total"
    static let lastPage = "last_page"
    static let perPage = "per_page"
    static let to = "to"
    static let currentPage = "current_page"
  }

  // MARK: Properties
  public var data: [ChatData]?
  public var from: Int?
  public var total: Int?
  public var lastPage: Int?
  public var perPage: Int?
  public var to: Int?
  public var currentPage: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    data <- map[SerializationKeys.data]
    from <- map[SerializationKeys.from]
    total <- map[SerializationKeys.total]
    lastPage <- map[SerializationKeys.lastPage]
    perPage <- map[SerializationKeys.perPage]
    to <- map[SerializationKeys.to]
    currentPage <- map[SerializationKeys.currentPage]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = data { dictionary[SerializationKeys.data] = value.map { $0.dictionaryRepresentation() } }
    if let value = from { dictionary[SerializationKeys.from] = value }
    if let value = total { dictionary[SerializationKeys.total] = value }
    if let value = lastPage { dictionary[SerializationKeys.lastPage] = value }
    if let value = perPage { dictionary[SerializationKeys.perPage] = value }
    if let value = to { dictionary[SerializationKeys.to] = value }
    if let value = currentPage { dictionary[SerializationKeys.currentPage] = value }
    return dictionary
  }

}
