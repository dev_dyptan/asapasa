//
//  ConversationsResponse.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class ConversationsResponse: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let cUsers = "c_users"
    static let conversationId = "conversation_id"
    static let createdAt = "created_at"
    static let sender = "sender"
    static let notification = "notification"
    static let body = "body"
    static let userId = "user_id"
    static let type = "type"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var id: Int?
  public var cUsers: [CUsers]?
  public var conversationId: Int?
  public var createdAt: String?
  public var sender: ConvSender?
  public var notification: [Any]?
  public var body: String?
  public var userId: Int?
  public var type: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    cUsers <- map[SerializationKeys.cUsers]
    conversationId <- map[SerializationKeys.conversationId]
    createdAt <- map[SerializationKeys.createdAt]
    sender <- map[SerializationKeys.sender]
    notification <- map[SerializationKeys.notification]
    body <- map[SerializationKeys.body]
    userId <- map[SerializationKeys.userId]
    type <- map[SerializationKeys.type]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = cUsers { dictionary[SerializationKeys.cUsers] = value.map { $0.dictionaryRepresentation() } }
    if let value = conversationId { dictionary[SerializationKeys.conversationId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = sender { dictionary[SerializationKeys.sender] = value.dictionaryRepresentation() }
    if let value = notification { dictionary[SerializationKeys.notification] = value }
    if let value = body { dictionary[SerializationKeys.body] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    return dictionary
  }

}
