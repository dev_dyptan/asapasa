//
//  Data.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class ChatData: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let offerId = "offer_id"
    static let body = "body"
    static let taskId = "task_id"
    static let conversationId = "conversation_id"
    static let sender = "sender"
    static let createdAt = "created_at"
    static let isSeen = "is_seen"
    static let userId = "user_id"
    static let type = "type"
    static let isSender = "is_sender"
  }

  // MARK: Properties
  public var offerId: Int?
  public var body: String?
  public var taskId: Int?
  public var conversationId: Int?
  public var sender: ChatSender?
  public var createdAt: String?
  public var isSeen: Int?
  public var userId: Int?
  public var type: String?
  public var isSender: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    offerId <- map[SerializationKeys.offerId]
    body <- map[SerializationKeys.body]
    taskId <- map[SerializationKeys.taskId]
    conversationId <- map[SerializationKeys.conversationId]
    sender <- map[SerializationKeys.sender]
    createdAt <- map[SerializationKeys.createdAt]
    isSeen <- map[SerializationKeys.isSeen]
    userId <- map[SerializationKeys.userId]
    type <- map[SerializationKeys.type]
    isSender <- map[SerializationKeys.isSender]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = offerId { dictionary[SerializationKeys.offerId] = value }
    if let value = body { dictionary[SerializationKeys.body] = value }
    if let value = taskId { dictionary[SerializationKeys.taskId] = value }
    if let value = conversationId { dictionary[SerializationKeys.conversationId] = value }
    if let value = sender { dictionary[SerializationKeys.sender] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = isSeen { dictionary[SerializationKeys.isSeen] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = isSender { dictionary[SerializationKeys.isSender] = value }
    return dictionary
  }

}
