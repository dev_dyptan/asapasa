//
//  Data.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class ProfileData: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let ratingAvg = "rating_avg"
    static let email = "email"
    static let pushNotifications = "push_notifications"
    static let successTasksAsCourier = "success_tasks_as_courier"
    static let emailNotifications = "email_notifications"
    static let phone = "phone"
    static let avatar = "avatar"
    static let reviewsAsCustomer = "reviewsAsCustomer"
    static let successTasksAsCustomer = "success_tasks_as_customer"
    static let reviewsAsCourier = "reviewsAsCourier"
  }

  // MARK: Properties
  public var name: String?
  public var ratingAvg: String?
  public var email: String?
  public var pushNotifications: Int?
  public var successTasksAsCourier: Int?
  public var emailNotifications: Int?
  public var phone: String?
  public var avatar: String?
  public var reviewsAsCustomer: [ReviewsAsCustomer]?
  public var successTasksAsCustomer: Int?
  public var reviewsAsCourier: [Any]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    ratingAvg <- map[SerializationKeys.ratingAvg]
    email <- map[SerializationKeys.email]
    pushNotifications <- map[SerializationKeys.pushNotifications]
    successTasksAsCourier <- map[SerializationKeys.successTasksAsCourier]
    emailNotifications <- map[SerializationKeys.emailNotifications]
    phone <- map[SerializationKeys.phone]
    avatar <- map[SerializationKeys.avatar]
    reviewsAsCustomer <- map[SerializationKeys.reviewsAsCustomer]
    successTasksAsCustomer <- map[SerializationKeys.successTasksAsCustomer]
    reviewsAsCourier <- map[SerializationKeys.reviewsAsCourier]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = ratingAvg { dictionary[SerializationKeys.ratingAvg] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = pushNotifications { dictionary[SerializationKeys.pushNotifications] = value }
    if let value = successTasksAsCourier { dictionary[SerializationKeys.successTasksAsCourier] = value }
    if let value = emailNotifications { dictionary[SerializationKeys.emailNotifications] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = avatar { dictionary[SerializationKeys.avatar] = value }
    if let value = reviewsAsCustomer { dictionary[SerializationKeys.reviewsAsCustomer] = value.map { $0.dictionaryRepresentation() } }
    if let value = successTasksAsCustomer { dictionary[SerializationKeys.successTasksAsCustomer] = value }
    if let value = reviewsAsCourier { dictionary[SerializationKeys.reviewsAsCourier] = value }
    return dictionary
  }

}
