//
//  Sender.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class ProfileSender: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let avatar = "avatar"
    static let name = "name"
    static let id = "id"
    static let links = "_links"
  }

  // MARK: Properties
  public var avatar: String?
  public var name: String?
  public var id: Int?
  public var links: ProfileLinks?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    avatar <- map[SerializationKeys.avatar]
    name <- map[SerializationKeys.name]
    id <- map[SerializationKeys.id]
    links <- map[SerializationKeys.links]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = avatar { dictionary[SerializationKeys.avatar] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    return dictionary
  }

}
