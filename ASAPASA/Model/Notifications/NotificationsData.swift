//
//  Data.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class NotificationsData: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let event = "event"
    static let eventId = "event_id"
    static let body = "body"
  }

  // MARK: Properties
  public var event: String?
  public var eventId: Int?
  public var body: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    event <- map[SerializationKeys.event]
    eventId <- map[SerializationKeys.eventId]
    body <- map[SerializationKeys.body]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = event { dictionary[SerializationKeys.event] = value }
    if let value = eventId { dictionary[SerializationKeys.eventId] = value }
    if let value = body { dictionary[SerializationKeys.body] = value }
    return dictionary
  }

}
