//
//  Meta.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class Meta: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pagination = "pagination"
  }

  // MARK: Properties
  public var pagination: Pagination?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    pagination <- map[SerializationKeys.pagination]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    return dictionary
  }

}
