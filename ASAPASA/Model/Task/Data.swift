//
//  Data.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class Data: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let delivery = "delivery"
    static let pickup = "pickup"
    static let deliveryTimeFrom = "delivery_time_from"
    static let links = "_links"
    static let customer = "customer"
    static let price = "price"
    static let stateId = "state_id"
    static let hasNotifications = "has_notifications"
    static let id = "id"
    static let distance = "distance"
    static let title = "title"
    static let images = "images"
    static let deliveryTimeTo = "delivery_time_to"
    static let sizeId = "size_id"
    static let deliveryDate = "delivery_date"
  }

  // MARK: Properties
  public var delivery: Delivery?
  public var pickup: Pickup?
  public var deliveryTimeFrom: String?
  public var links: TaskLinks?
  public var customer: Customer?
  public var price: Float?
  public var stateId: Int?
  public var hasNotifications: Bool? = false
  public var id: Int?
  public var distance: Float?
  public var title: String?
  public var images: [Images]?
  public var deliveryTimeTo: String?
  public var sizeId: Int?
  public var deliveryDate: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    delivery <- map[SerializationKeys.delivery]
    pickup <- map[SerializationKeys.pickup]
    deliveryTimeFrom <- map[SerializationKeys.deliveryTimeFrom]
    links <- map[SerializationKeys.links]
    customer <- map[SerializationKeys.customer]
    price <- map[SerializationKeys.price]
    stateId <- map[SerializationKeys.stateId]
    hasNotifications <- map[SerializationKeys.hasNotifications]
    id <- map[SerializationKeys.id]
    distance <- map[SerializationKeys.distance]
    title <- map[SerializationKeys.title]
    images <- map[SerializationKeys.images]
    deliveryTimeTo <- map[SerializationKeys.deliveryTimeTo]
    sizeId <- map[SerializationKeys.sizeId]
    deliveryDate <- map[SerializationKeys.deliveryDate]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = delivery { dictionary[SerializationKeys.delivery] = value.dictionaryRepresentation() }
    if let value = pickup { dictionary[SerializationKeys.pickup] = value.dictionaryRepresentation() }
    if let value = deliveryTimeFrom { dictionary[SerializationKeys.deliveryTimeFrom] = value }
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    if let value = customer { dictionary[SerializationKeys.customer] = value.dictionaryRepresentation() }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = stateId { dictionary[SerializationKeys.stateId] = value }
    dictionary[SerializationKeys.hasNotifications] = hasNotifications
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = distance { dictionary[SerializationKeys.distance] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = images { dictionary[SerializationKeys.images] = value.map { $0.dictionaryRepresentation() } }
    if let value = deliveryTimeTo { dictionary[SerializationKeys.deliveryTimeTo] = value }
    if let value = sizeId { dictionary[SerializationKeys.sizeId] = value }
    if let value = deliveryDate { dictionary[SerializationKeys.deliveryDate] = value }
    return dictionary
  }

}
