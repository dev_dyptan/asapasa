//
//  TypeEnum.swift
//  ASAPASA
//
//  Created by Lera on 04.05.17.
//  Copyright © 2017 Dev. All rights reserved.
//

import Foundation

enum Type: String {
    case Active = "active"
    case Waiting = "waiting"
    case Finished = "finished"
}
