//
//  Pickup.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class Pickup: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let city = "city"
    static let location = "location"
    static let region = "region"
    static let street = "street"
    static let lon = "lon"
    static let lat = "lat"
    static let country = "country"
  }

  // MARK: Properties
  public var city: String?
  public var location: String?
  public var region: String?
  public var street: String?
  public var lon: Float?
  public var lat: Float?
  public var country: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    city <- map[SerializationKeys.city]
    location <- map[SerializationKeys.location]
    region <- map[SerializationKeys.region]
    street <- map[SerializationKeys.street]
    lon <- map[SerializationKeys.lon]
    lat <- map[SerializationKeys.lat]
    country <- map[SerializationKeys.country]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = city { dictionary[SerializationKeys.city] = value }
    if let value = location { dictionary[SerializationKeys.location] = value }
    if let value = region { dictionary[SerializationKeys.region] = value }
    if let value = street { dictionary[SerializationKeys.street] = value }
    if let value = lon { dictionary[SerializationKeys.lon] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = country { dictionary[SerializationKeys.country] = value }
    return dictionary
  }

}
