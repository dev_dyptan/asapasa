//
//  Images.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class Images: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let path = "path"
    static let thumb = "thumb"
    static let uuid = "uuid"
  }

  // MARK: Properties
  public var path: String?
  public var thumb: String?
  public var uuid: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    path <- map[SerializationKeys.path]
    thumb <- map[SerializationKeys.thumb]
    uuid <- map[SerializationKeys.uuid]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = path { dictionary[SerializationKeys.path] = value }
    if let value = thumb { dictionary[SerializationKeys.thumb] = value }
    if let value = uuid { dictionary[SerializationKeys.uuid] = value }
    return dictionary
  }

}
