//
//  Pagination.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class Pagination: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let total = "total"
    static let totalPages = "total_pages"
    static let links = "links"
    static let perPage = "per_page"
    static let currentPage = "current_page"
    static let count = "count"
  }

  // MARK: Properties
  public var total: Int?
  public var totalPages: Int?
  public var links: [Any]?
  public var perPage: Int?
  public var currentPage: Int?
  public var count: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    total <- map[SerializationKeys.total]
    totalPages <- map[SerializationKeys.totalPages]
    links <- map[SerializationKeys.links]
    perPage <- map[SerializationKeys.perPage]
    currentPage <- map[SerializationKeys.currentPage]
    count <- map[SerializationKeys.count]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = total { dictionary[SerializationKeys.total] = value }
    if let value = totalPages { dictionary[SerializationKeys.totalPages] = value }
    if let value = links { dictionary[SerializationKeys.links] = value }
    if let value = perPage { dictionary[SerializationKeys.perPage] = value }
    if let value = currentPage { dictionary[SerializationKeys.currentPage] = value }
    if let value = count { dictionary[SerializationKeys.count] = value }
    return dictionary
  }

}
