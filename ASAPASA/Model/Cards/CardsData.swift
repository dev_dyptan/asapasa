//
//  Data.swift
//
//  Created by Colt on 04.05.17
//  Copyright (c) . All rights reserved.
//

import ObjectMapper

public class CardsData: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let links = "_links"
    static let last4 = "last4"
    static let isDefault = "is_default"
    static let id = "id"
    static let createdAt = "created_at"
  }

  // MARK: Properties
  public var links: CardsLinks?
  public var last4: String?
  public var isDefault: Int?
  public var id: Int?
  public var createdAt: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    links <- map[SerializationKeys.links]
    last4 <- map[SerializationKeys.last4]
    isDefault <- map[SerializationKeys.isDefault]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = links { dictionary[SerializationKeys.links] = value.dictionaryRepresentation() }
    if let value = last4 { dictionary[SerializationKeys.last4] = value }
    if let value = isDefault { dictionary[SerializationKeys.isDefault] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

}
